import { useMemo } from "react";
import { createContext } from "react";
import { FrappeApp } from "frappe-js-sdk";
import { useCallback, useContext, useEffect, useState } from 'react'

export const FrappeContext = createContext(null)

export const FrappeProvider = ({ url = "", tokenParams, children }) => {

    const frappeConfig = useMemo(() => {
        //Add your Frappe backend's URL
        const frappe = new FrappeApp(url, tokenParams)

        return {
            url,
            tokenParams,
            app: frappe,
            auth: frappe.auth(),
            db: frappe.db(),
            call: frappe.call(),
            file: frappe.file()
        }

    }, [url, tokenParams])

    return <FrappeContext.Provider value={frappeConfig}>{children}</FrappeContext.Provider>
}

/**
 * Hook to start listening to user state and provides functions to login/logout
 * 
 * @param options - [Optional] SWRConfiguration options for fetching current logged in user
 * @returns Returns an object with the following properties: currentUser, loading, error, and functions to login, logout and updateCurrentUser
 */
export const useFrappeAuth = (options) => {

    const { url, auth, tokenParams } = useContext(FrappeContext)

    const [userID, setUserID] = useState()
    const [privateCookie, setPrivateCookie] = useState()
    const [document, setDocument] = useState()

    const getUserCookie = useCallback(async () => {
        const cookie = document?.cookie?.length > 0 ? document.cookie[0] : ''
        setPrivateCookie(cookie)
        const userCookie = cookie.split(';').find(c => c.trim().startsWith('user_id='))
        if (userCookie) {
            const userName = userCookie.split('=')[1]
            if (userName && userName !== "Guest") {
                setUserID(userName)
            } else {
                setUserID(null)
            }
        } else {
            setUserID(null)
        }
        
    }, [])

    useEffect(() => {
        //Only get user cookie if token is not used
        if (tokenParams && tokenParams.useToken) {
            setUserID(null)
        } else {
            getUserCookie()
        }

    }, [])

    const login = useCallback(async (username, password) => {
        return auth.loginWithUsernamePassword({ username, password }).then((m) => {
            setDocument(m)
            getUserCookie()
        })
    }, [])

    const logout = useCallback(async () => {
        return auth.logout()
            .then(() => updateCurrentUser(null))
            .then(() => setUserID(null))
    }, [])

    return {
        login,
        logout
    }
}

export const getRequestURL = (doctype, url, docname) => {
    let requestURL = `${url}/api/resource/`;
    if (docname) {
        requestURL += `${doctype}/${docname}`;
    } else {
        requestURL += `${doctype}`;
    }

    return requestURL;
}

/**
 * Hook to fetch a document from the database
 * 
 * 
 * @param doctype - The doctype to fetch
 * @param name - the name of the document to fetch
 * @param options [Optional] SWRConfiguration options for fetching data
 * @returns an object (SWRResponse) with the following properties: data, error, isValidating, and mutate
 * 
 * @typeParam T - The type of the document
 */
export const useFrappeGetDoc = () => {

    const { db } = useContext(FrappeContext)

    const getDoc = useCallback(async (doctype, name) => {
        return db.getDoc(doctype, name)
            .then((document) => {
                return document
            })
            .catch((error) => {
                throw error
            })
    }, [])

    return {
        getDoc
    }
}
/**
 * Hook to fetch a list of documents from the database
 * 
 * @param doctype Name of the doctype to fetch
 * @param args Arguments to pass (filters, pagination, etc)
 * @param options [Optional] SWRConfiguration options for fetching data
 * @returns an object (SWRResponse) with the following properties: data, error, isValidating, and mutate
 * 
* @typeParam T - The type definition of the document object
 */
export const useFrappeGetDocList =  () => {

    const { db } = useContext(FrappeContext)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const [isCompleted, setIsCompleted] = useState(false)

    const getDocList = useCallback(async (doctype, args) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)

        return db.getDocList(doctype, args)
            .then((document) => {
                setLoading(false)
                setIsCompleted(true)
                return document
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])


    return {
        getDocList,
        loading,
        error,
        isCompleted
    }
}

/**
 * Hook to create a document in the database and maintain loading and error states
 * @returns Object with the following properties: loading, error, isCompleted and createDoc and reset functions
 */
export const useFrappeCreateDoc = () => {

    const { db } = useContext(FrappeContext)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const [isCompleted, setIsCompleted] = useState(false)

    const reset = useCallback(() => {
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    const createDoc = useCallback(async (doctype, doc) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)

        return db.createDoc(doctype, doc)
            .then((document) => {
                setLoading(false)
                setIsCompleted(true)
                return document
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])

    return {
        createDoc,
        loading,
        error,
        isCompleted,
        reset
    }
}

/**
 * Hook to update a document in the database and maintain loading and error states
 * @returns Object with the following properties: loading, error, isCompleted and updateDoc and reset functions
 */
export const useFrappeUpdateDoc = () => {

    const { db } = useContext(FrappeContext)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const [isCompleted, setIsCompleted] = useState(false)



    const reset = useCallback(() => {
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    const updateDoc = useCallback(async (doctype, docname, doc) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)
        return db.updateDoc(doctype, docname, doc)
            .then((document) => {
                setLoading(false)
                setIsCompleted(true)
                return document
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])

    return {
        updateDoc,
        loading,
        error,
        reset,
        isCompleted
    }
}

/**
 * Hook to delete a document in the database and maintain loading and error states
 * @returns Object with the following properties: loading, error, isCompleted and deleteDoc and reset functions
 */
export const useFrappeDeleteDoc = () => {

    const { db } = useContext(FrappeContext)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const [isCompleted, setIsCompleted] = useState(false)



    const reset = useCallback(() => {
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    const deleteDoc = useCallback(async (doctype, docname) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)

        return db.deleteDoc(doctype, docname)
            .then((message) => {
                setLoading(false)
                setIsCompleted(true)
                return message
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])

    return {
        deleteDoc,
        loading,
        error,
        reset,
        isCompleted
    }
}

function encodeQueryData(data) {
    const ret = [];
    for (let d in data)
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    return ret.join('&');
}

/**
 *  Hook to make a GET request to the server
 * 
 * @param method - name of the method to call (will be dotted path e.g. "frappe.client.get_list")
 * @param params - parameters to pass to the method
 * @param swrKey - optional SWRKey that will be used to cache the result. If not provided, the method name with the URL params will be used as the key
 * @param options [Optional] SWRConfiguration options for fetching data
 * 
 * @typeParam T - Type of the data returned by the method
 * @returns an object (SWRResponse) with the following properties: data (number), error, isValidating, and mutate
 */
export const useFrappeGetCall = (method, params) => {

    const { call } = useContext(FrappeContext)

    return call.get(method, params)
            .then((message) => {
                return message
            })
            .catch((error) => {
                throw error
            })
}

/**
 * 
 * @param method - name of the method to call (POST request) (will be dotted path e.g. "frappe.client.set_value")
 * @returns an object with the following properties: loading, error, isCompleted , result, and call and reset functions
 */
export const useFrappePostCall = (method) => {

    const { call: frappeCall } = useContext(FrappeContext)

    const [result, setResult] = useState(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const [isCompleted, setIsCompleted] = useState(false)



    const reset = useCallback(() => {
        setResult(null)
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    const call = useCallback(async (params) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)
        setResult(null)

        return frappeCall.post(method, params)
            .then((message) => {
                setResult(message)
                setLoading(false)
                setIsCompleted(true)
                return message
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])

    return {
        call,
        result,
        loading,
        error,
        reset,
        isCompleted
    }
}

/**
 * 
 * @param method - name of the method to call (PUT request) (will be dotted path e.g. "frappe.client.set_value")
 * @returns an object with the following properties: loading, error, isCompleted , result, and call and reset functions
 */
export const useFrappePutCall = (method) => {

    const { call: frappeCall } = useContext(FrappeContext)

    const [result, setResult] = useState<T | null>(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState<Error | null>(null)
    const [isCompleted, setIsCompleted] = useState(false)



    const reset = useCallback(() => {
        setResult(null)
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    const call = useCallback(async (params) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)
        setResult(null)

        return frappeCall.put(method, params)
            .then((message) => {
                setResult(message)
                setLoading(false)
                setIsCompleted(true)
                return message
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])

    return {
        call,
        result,
        loading,
        error,
        reset,
        isCompleted
    }
}

/**
 * 
 * @param method - name of the method to call (DELETE request) (will be dotted path e.g. "frappe.client.delete")
 * @returns an object with the following properties: loading, error, isCompleted , result, and call and reset functions
 */
export const useFrappeDeleteCall = (method) => {

    const { call: frappeCall } = useContext(FrappeContext)

    const [result, setResult] = useState(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const [isCompleted, setIsCompleted] = useState(false)



    const reset = useCallback(() => {
        setResult(null)
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    const call = useCallback(async (params) => {
        setError(null)
        setIsCompleted(false)
        setLoading(true)
        setResult(null)

        return frappeCall.delete(method, params)
            .then((message) => {
                setResult(message)
                setLoading(false)
                setIsCompleted(true)
                return message
            })
            .catch((error) => {
                setLoading(false)
                setIsCompleted(false)
                setError(error)
                throw error
            })
    }, [])

    return {
        call,
        result,
        loading,
        error,
        reset,
        isCompleted
    }
}

/**
 * Hook to upload files to the server
 * 
 * @returns an object with the following properties: loading, error, isCompleted , result, and call and reset functions
 */
export const useFrappeFileUpload = () => {

    const { file } = useContext(FrappeContext)
    const [progress, setProgress] = useState(0)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState<Error | null>(null)
    const [isCompleted, setIsCompleted] = useState(false)

    const upload = useCallback(async (f, args, apiPath) => {
        reset()
        setLoading(true)
        return file.uploadFile(f, args, (c, t) => setProgress(Math.round((c / t) * 100)), apiPath)
            .then((r) => {
                setIsCompleted(true)
                setProgress(100)
                setLoading(false)
                return r.data.message
            })
            .catch(e => {
                console.error(e)
                setError(e)
                setLoading(false)
                throw e
            })

    }, [])

    const reset = useCallback(() => {
        setProgress(0)
        setLoading(false)
        setError(null)
        setIsCompleted(false)
    }, [])

    return {
        upload,
        progress,
        loading,
        isCompleted,
        error,
        reset
    }

}


/**
 * Hook to search for documents
 * 
  * @param doctype - name of the doctype (table) where we are performing our search
  * @param text - search text
  * @param filters - (optional) the results will be filtered based on these 
  * @param limit - (optional) the number of results to return. Defaults to 20
  * @param debounce - (optional) the number of milliseconds to wait before making the API call. Defaults to 250ms.
  * @returns result - array of type SearchResult with a list of suggestions based on search text
  */
export const useSearch = async (doctype, text, filters = [], limit = 20, debounce = 250) => {
    const debouncedText = useDebounce(text, debounce);
    const swrResult = await useFrappeGetCall('frappe.desk.search.search_link', {
        doctype,
        page_length: limit,
        txt: debouncedText,
        filters: JSON.stringify(filters ?? [])
    })
    return swrResult
}

/**
 * Hook to debounce user input
 * @param value - the value to be debounced
 * @param delay - the number of milliseconds to wait before returning the value
 * @returns string value after the specified delay
 */
const useDebounce = (value, delay) => {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        return () => {
            clearTimeout(handler);
        };
    }, [value, delay]);

    return debouncedValue;
}


/* ---- Socket IO Hooks ---- */
/** useFrappeEventListener hook for listening to events from the server
 * @param eventName - name of the event to listen to
 * @param callback - callback function to be called when the event is triggered. The callback function will receive the data sent from the server. It is recommended to memoize this function.
 * 
 * @example
 * ```typescript
 * useFrappeEventListener('my_event', (data) => {
 *     // do something with the data
 *      if(data.status === 'success') {
 *          console.log('success')
 *      }
 * })
 * ```
 */
export const useFrappeEventListener = (eventName, callback) => {

    const { socket } = useContext(FrappeContext)

    useEffect(() => {
        if(socket === undefined){
            console.warn('Socket is not enabled. Please enable socket in FrappeProvider.')
        }
        let listener = socket?.on(eventName, callback)

        return () => {
            listener?.off(eventName)
        }
    }, [eventName, callback])

}

/**
 * Hook for listening to document events.
 * The hook will automatically subscribe to the document room, and unsubscribe when the component unmounts.
 * The hook listens to the following events:
 * - doc_update: This is triggered when the document is updated. The callback function will receive the updated document.
 * - doc_viewers: This is triggered when the list of viewers of the document changes. The hook will update the viewers state with the list of viewers.
 * 
 * @param doctype Name of the doctype
 * @param docname Name of the document
 * @param emitOpenCloseEventsOnMount [Optional] If true, the hook will emit doc_open and doc_close events on mount and unmount respectively. Defaults to true.
 * @param onUpdateCallback Function to be called when the document is updated. It is recommended to memoize this function.
 * @returns viewers - array of userID's, emitDocOpen - function to emit doc_open event, emitDocClose - function to emit doc_close event
 */
export const useFrappeDocumentEventListener = (
    doctype,
    docname,
    onUpdateCallback,
    emitOpenCloseEventsOnMount = true,
) => {
    const { socket } = useContext(FrappeContext)

    /** Array of user IDs of users currently viewing the document. This is updated when "doc_viewers" event is published */
    const [viewers, setViewers] = useState([])

    useEffect(() => {
        if(socket === undefined){
            console.warn('Socket is not enabled. Please enable socket in FrappeProvider.')
        }
        socket?.emit('doc_subscribe', doctype, docname)
        if (emitOpenCloseEventsOnMount) {
            socket?.emit('doc_open', doctype, docname)
        }

        return () => {
            socket?.emit('doc_unsubscribe', doctype, docname)
            if (emitOpenCloseEventsOnMount) {
                socket?.emit('doc_close', doctype, docname)
            }
        }
    }, [doctype, docname, emitOpenCloseEventsOnMount]);

    useFrappeEventListener('doc_update', onUpdateCallback)

    /**
     * Emit doc_open event - this will explicitly send a doc_open event to the server.
     */
    const emitDocOpen = useCallback(() => {
        socket?.emit('doc_open', doctype, docname)
    }, [doctype, docname])

    /**
     * Emit doc_close event - this will explicitly send a doc_close event to the server.
     */
    const emitDocClose = useCallback(() => {
        socket?.emit('doc_close', doctype, docname)
    }, [doctype, docname])

    const onViewerEvent = useCallback((data) => {
        if (data.doctype === doctype && data.docname === docname) {
            setViewers(data.users)
        }
    }, [doctype, docname])

    useFrappeEventListener('doc_viewers', onViewerEvent)

    return {
        /** Array of user IDs of users currently viewing the document. This is updated when "doc_viewers" event is published */
        viewers,
        /** Emit doc_open event - this will explicitly send a doc_open event to the server. */
        emitDocOpen,
        /** Emit doc_close event - this will explicitly send a doc_close event to the server. */
        emitDocClose,
    }

}


/**
 * Hook for listening to doctype events.
 * The hook will automatically subscribe to the doctype room, and unsubscribe when the component unmounts.
 * The hook listens to the following event:
 * - list_update: This is triggered when a document of the doctype is updated (created, modified or deleted). The callback function will receive the updated document.
 * 
 * @param doctype Name of the doctype
 * @param onListUpdateCallback Function to be called when the document is updated. It is recommended to memoize this function.
 */
export const useFrappeDocTypeEventListener = (
    doctype,
    onListUpdateCallback,
) => {
    const { socket } = useContext(FrappeContext)

    useEffect(() => {
        if(socket === undefined){
            console.warn('Socket is not enabled. Please enable socket in FrappeProvider.')
        }
        socket?.emit('doctype_subscribe', doctype)
        return () => {
            socket?.emit('doctype_unsubscribe', doctype)
        }
    }, [doctype]);

    useFrappeEventListener('list_update', onListUpdateCallback)

}