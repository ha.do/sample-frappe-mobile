import React from "react";

import LoginScreen from "./src/login/login.js";
import { FrappeProvider } from "./libs/FrappeSDK";

const getTokenFromLocalStorage = () => {
  return "sid=0b24df7fa198869db275276c40131fde07f31e6a5c9ecdf43046dfe3; Expires=Thu, 12 Oct 2023 15:05:53 GMT; Secure; HttpOnly; Path=/; SameSite=Lax"
}

export default function App() {
  return (
    <FrappeProvider url='https://ocrm.oshima.vn' tokenParams={{
      useToken: true,
      token: getTokenFromLocalStorage(),
      type: "cookie"
    }}>
    <LoginScreen />
    </FrappeProvider>
  )
}
