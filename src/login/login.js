import React from "react";
import { useFrappeAuth, useFrappeGetDocList } from "../../libs/FrappeSDK";

import styles from "./style";
import {
  Alert,
  Keyboard,
  KeyboardAvoidingView,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Button, SocialIcon } from "react-native-elements";
import * as Facebook from "expo-facebook";

const appId = "1047121222092614";

export default function LoginScreen() {
  const {
    currentUser,
    isValidating,
    isLoading,
    login,
    logout,
    error,
    updateCurrentUser,
    getUserCookie,
  } = useFrappeAuth();
  const {
    getDocList
  } = useFrappeGetDocList();

  const onLoginPress = () => {
    login('Administrator', '@BaoUyen')
  };

  const onFbLoginPress = async () => {
    getDocList(
      'Web Page',
      {
        fields: ['name', 'creation'],
        filters: [['creation', '>', '2023-10-01']],
        limit_start: 5,
        limit: 10,
        orderBy: {
          field: 'creation',
          order: 'desc',
        },
        asDict: false,
      }
    );
  };

  

  return (
    <KeyboardAvoidingView style={styles.containerView} behavior="padding">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.loginScreenContainer}>
          <View style={styles.loginFormView}>
            
            <Text style={styles.logoText}>Login</Text>
            <TextInput
              placeholder="Username"
              placeholderColor="#c4c3cb"
              style={styles.loginFormTextInput}
            />
            <TextInput
              placeholder="Password"
              placeholderColor="#c4c3cb"
              style={styles.loginFormTextInput}
              secureTextEntry={true}
            />
            <Button
              buttonStyle={styles.loginButton}
              onPress={() => onLoginPress()}
              title="Login"
            />
            <Button
              containerStyle={styles.fbLoginButton}
              type="clear"
              onPress={() => onFbLoginPress()}
              title="Fetch sample document"
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
